<!DOCTYPE html>

<html lang="tr" prefix="og: https://ogp.me/ns# article: https://ogp.me/ns/article#">

<!-- Mirrored from vdemir.github.io/Lisan/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Jun 2021 13:12:58 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <link rel="openid.delegate" href="../index.html" />
   <link rel="openid.server" href="https://openid.indieauth.com/openid" />
   <link rel='author' href='../humans.txt' type='text/plain'>
   <link rel="icon" type="image/x-icon" href="../images/icon.png">
   <!--<link rel="canonical" href="https://vdemir.github.io/Lisan/">-->
   <link rel="canonical" href="index.html">
    <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
<!-- seo ok
    <title>Lisan Yordamı ile Dil Öğrenme Metodu &mdash; Daily</title>

    
    <meta name="description" content="This post is about Language">
    <meta name="og:description" content="This post is about Language">
    
-->
    <link rel="self" href="../index.html"/>
    
    <link href="../feed.xml" type="application/atom+xml" rel="alternate" title="Günce - Günlük Blog Yazıları Feed">

    
    <meta name="keywords" content="lisan, ingilizce, dil" />
    
    
     <!-- Begin Jekyll SEO tag v2.7.1 -->
<title>Lisan Yordamı ile Dil Öğrenme Metodu | Günce - Günlük Blog Yazıları</title>
<meta name="generator" content="Jekyll v3.9.0" />
<meta property="og:title" content="Lisan Yordamı ile Dil Öğrenme Metodu" />
<meta name="author" content="Günce" />
<meta property="og:locale" content="tr" />
<meta name="description" content="This post is about Language" />
<meta property="og:description" content="This post is about Language" />
<link rel="canonical" href="index.html" />
<meta property="og:url" content="index.html" />
<meta property="og:site_name" content="Günce - Günlük Blog Yazıları" />
<meta property="og:type" content="article" />
<meta property="article:published_time" content="2018-09-11T15:00:00+03:00" />
<meta name="twitter:card" content="summary" />
<meta property="twitter:title" content="Lisan Yordamı ile Dil Öğrenme Metodu" />
<meta name="google-site-verification" content="BCcxBcS3lgMpeUrrxaTxXkRz0IqUa5Jp4-N6GYIB7VM" />
<script type="application/ld+json">
{"author":{"@type":"Person","name":"Günce"},"mainEntityOfPage":{"@type":"WebPage","@id":"https://vdemir.github.io/Lisan/"},"description":"This post is about Language","url":"https://vdemir.github.io/Lisan/","@type":"BlogPosting","publisher":{"@type":"Organization","logo":{"@type":"ImageObject","url":"https://vdemir.github.io/siteicon.png"},"name":"Günce"},"headline":"Lisan Yordamı ile Dil Öğrenme Metodu","dateModified":"2018-09-11T15:00:00+03:00","datePublished":"2018-09-11T15:00:00+03:00","@context":"https://schema.org"}</script>
<!-- End Jekyll SEO tag -->

     

  








  
<style>
/* Put nice boxes around each algorithm. */
[data-algorithm]:not(.heading) {
  padding: .5em;
  border: thin solid #ddd; border-radius: .5em;
  margin: .5em calc(-0.5em - 1px);
}
[data-algorithm]:not(.heading) > :first-child {
  margin-top: 0;
}
[data-algorithm]:not(.heading) > :last-child {
  margin-bottom: 0;
}
[data-algorithm] [data-algorithm] {
    margin: 1em 0;
}
</style>
<style>/* style-md-lists */

            /* This is a weird hack for me not yet following the commonmark spec
               regarding paragraph and lists. */
            [data-md] > :first-child {
                margin-top: 0;
            }
            [data-md] > :last-child {
                margin-bottom: 0;
            }</style>
<style>/* style-selflinks */

            .heading, .issue, .note, .example, li, dt {
                position: relative;
            }
            a.self-link {
                position: absolute;
                top: 0;
                left: calc(-1 * (3.5rem - 26px));
                width: calc(3.5rem - 26px);
                height: 2em;
                text-align: center;
                border: none;
                transition: opacity .2s;
                opacity: .5;
            }
            a.self-link:hover {
                opacity: 1;
            }
            .heading > a.self-link {
                font-size: 83%;
            }
            li > a.self-link {
                left: calc(-1 * (3.5rem - 26px) - 2em);
            }
            dfn > a.self-link {
                top: auto;
                left: auto;
                opacity: 0;
                width: 1.5em;
                height: 1.5em;
                background: gray;
                color: white;
                font-style: normal;
                transition: opacity .2s, background-color .2s, color .2s;
            }
            dfn:hover > a.self-link {
                opacity: 1;
            }
            dfn > a.self-link:hover {
                color: black;
            }

            a.self-link::before            { content: "¶"; }
            .heading > a.self-link::before { content: "§"; }
            dfn > a.self-link::before      { content: "#"; }</style>
<style>/* style-counters */

            body {
                counter-reset: example figure issue;
            }
            .issue {
                counter-increment: issue;
            }
            .issue:not(.no-marker)::before {
                content: "Issue " counter(issue);
            }

            .example {
                counter-increment: example;
            }
            .example:not(.no-marker)::before {
                content: "Example " counter(example);
            }
            .invalid.example:not(.no-marker)::before,
            .illegal.example:not(.no-marker)::before {
                content: "Invalid Example" counter(example);
            }

            figcaption {
                counter-increment: figure;
            }
            figcaption:not(.no-marker)::before {
                content: "Figure " counter(figure) " ";
            }</style>
<style>/* style-autolinks */

            .css.css, .property.property, .descriptor.descriptor {
                color: #005a9c;
                font-size: inherit;
                font-family: inherit;
            }
            .css::before, .property::before, .descriptor::before {
                content: "‘";
            }
            .css::after, .property::after, .descriptor::after {
                content: "’";
            }
            .property, .descriptor {
                /* Don't wrap property and descriptor names */
                white-space: nowrap;
            }
            .type { /* CSS value <type> */
                font-style: italic;
            }
            pre .property::before, pre .property::after {
                content: "";
            }
            [data-link-type="property"]::before,
            [data-link-type="propdesc"]::before,
            [data-link-type="descriptor"]::before,
            [data-link-type="value"]::before,
            [data-link-type="function"]::before,
            [data-link-type="at-rule"]::before,
            [data-link-type="selector"]::before,
            [data-link-type="maybe"]::before {
                content: "‘";
            }
            [data-link-type="property"]::after,
            [data-link-type="propdesc"]::after,
            [data-link-type="descriptor"]::after,
            [data-link-type="value"]::after,
            [data-link-type="function"]::after,
            [data-link-type="at-rule"]::after,
            [data-link-type="selector"]::after,
            [data-link-type="maybe"]::after {
                content: "’";
            }

            [data-link-type].production::before,
            [data-link-type].production::after,
            .prod [data-link-type]::before,
            .prod [data-link-type]::after {
                content: "";
            }

            [data-link-type=element],
            [data-link-type=element-attr] {
                font-family: Menlo, Consolas, "DejaVu Sans Mono", monospace;
                font-size: .9em;
            }
            [data-link-type=element]::before { content: "<" }
            [data-link-type=element]::after  { content: ">" }

            [data-link-type=biblio] {
                white-space: pre;
            }</style>
<style>/* style-dfn-panel */

        .dfn-panel {
            position: absolute;
            z-index: 35;
            height: auto;
            width: -webkit-fit-content;
            width: fit-content;
            max-width: 300px;
            max-height: 500px;
            overflow: auto;
            padding: 0.5em 0.75em;
            font: small Helvetica Neue, sans-serif, Droid Sans Fallback;
            background: #DDDDDD;
            color: black;
            border: outset 0.2em;
        }
        .dfn-panel:not(.on) { display: none; }
        .dfn-panel * { margin: 0; padding: 0; text-indent: 0; }
        .dfn-panel > b { display: block; }
        .dfn-panel a { color: black; }
        .dfn-panel a:not(:hover) { text-decoration: none !important; border-bottom: none !important; }
        .dfn-panel > b + b { margin-top: 0.25em; }
        .dfn-panel ul { padding: 0; }
        .dfn-panel li { list-style: inside; }
        .dfn-panel.activated {
            display: inline-block;
            position: fixed;
            left: .5em;
            bottom: 2em;
            margin: 0 auto;
            max-width: calc(100vw - 1.5em - .4em - .5em);
            max-height: 30vh;
        }

        .dfn-paneled { cursor: pointer; }
        </style>
<style>/* style-syntax-highlighting */
pre.idl.highlight { color: #708090; }
.highlight:not(.idl) { background: hsl(24, 20%, 95%); }
code.highlight { padding: .1em; border-radius: .3em; }
pre.highlight, pre > code.highlight { display: block; padding: 1em; margin: .5em 0; overflow: auto; border-radius: 0; }
.highlight .c { color: #708090 } /* Comment */
.highlight .k { color: #990055 } /* Keyword */
.highlight .l { color: #000000 } /* Literal */
.highlight .n { color: #0077aa } /* Name */
.highlight .o { color: #999999 } /* Operator */
.highlight .p { color: #999999 } /* Punctuation */
.highlight .cm { color: #708090 } /* Comment.Multiline */
.highlight .cp { color: #708090 } /* Comment.Preproc */
.highlight .c1 { color: #708090 } /* Comment.Single */
.highlight .cs { color: #708090 } /* Comment.Special */
.highlight .kc { color: #990055 } /* Keyword.Constant */
.highlight .kd { color: #990055 } /* Keyword.Declaration */
.highlight .kn { color: #990055 } /* Keyword.Namespace */
.highlight .kp { color: #990055 } /* Keyword.Pseudo */
.highlight .kr { color: #990055 } /* Keyword.Reserved */
.highlight .kt { color: #990055 } /* Keyword.Type */
.highlight .ld { color: #000000 } /* Literal.Date */
.highlight .m { color: #000000 } /* Literal.Number */
.highlight .s { color: #a67f59 } /* Literal.String */
.highlight .na { color: #0077aa } /* Name.Attribute */
.highlight .nc { color: #0077aa } /* Name.Class */
.highlight .no { color: #0077aa } /* Name.Constant */
.highlight .nd { color: #0077aa } /* Name.Decorator */
.highlight .ni { color: #0077aa } /* Name.Entity */
.highlight .ne { color: #0077aa } /* Name.Exception */
.highlight .nf { color: #0077aa } /* Name.Function */
.highlight .nl { color: #0077aa } /* Name.Label */
.highlight .nn { color: #0077aa } /* Name.Namespace */
.highlight .py { color: #0077aa } /* Name.Property */
.highlight .nt { color: #669900 } /* Name.Tag */
.highlight .nv { color: #222222 } /* Name.Variable */
.highlight .ow { color: #999999 } /* Operator.Word */
.highlight .mb { color: #000000 } /* Literal.Number.Bin */
.highlight .mf { color: #000000 } /* Literal.Number.Float */
.highlight .mh { color: #000000 } /* Literal.Number.Hex */
.highlight .mi { color: #000000 } /* Literal.Number.Integer */
.highlight .mo { color: #000000 } /* Literal.Number.Oct */
.highlight .sb { color: #a67f59 } /* Literal.String.Backtick */
.highlight .sc { color: #a67f59 } /* Literal.String.Char */
.highlight .sd { color: #a67f59 } /* Literal.String.Doc */
.highlight .s2 { color: #a67f59 } /* Literal.String.Double */
.highlight .se { color: #a67f59 } /* Literal.String.Escape */
.highlight .sh { color: #a67f59 } /* Literal.String.Heredoc */
.highlight .si { color: #a67f59 } /* Literal.String.Interpol */
.highlight .sx { color: #a67f59 } /* Literal.String.Other */
.highlight .sr { color: #a67f59 } /* Literal.String.Regex */
.highlight .s1 { color: #a67f59 } /* Literal.String.Single */
.highlight .ss { color: #a67f59 } /* Literal.String.Symbol */
.highlight .vc { color: #0077aa } /* Name.Variable.Class */
.highlight .vg { color: #0077aa } /* Name.Variable.Global */
.highlight .vi { color: #0077aa } /* Name.Variable.Instance */
.highlight .il { color: #000000 } /* Literal.Number.Integer.Long */
</style>
  <link href="../css/base2.css" rel="stylesheet" type="text/css">


   <meta name="robots" content="index,follow">
   <meta name="readability-verification" content="e3fb0316180ffe5e0e35411b93161b5083c1b0e7">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  
    <meta name="google-site-verification" content="zFuwF6tlf7YemTRN-ZOvl5lG1rjuI9qEomPbr_5vAu8" />
    <meta name="yandex-verification" content="63e9875cbd802327" />
    <link rel="search" type="application/opensearchdescription+xml" href="../opensearch.xml" title="$günce" />
 
 <!-- apple-icon-->
<link rel="apple-touch-icon" sizes="57x57" href="../apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="../android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="../favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
<link rel="manifest" href="../manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

     
<link rel='dns-prefetch' href='https://www.github.com/'>
<link rel='dns-prefetch' href='https://ajax.googleapis.com/'>
<link rel='dns-prefetch' href='https://counter2000.herokuapp.com/'>
  
<style>
.responsive-wrap a, p, div, img iframe{ max-width: 100%;}
</style>
</head>
<body>
   
   

 
   <!-- scroll CSS -->
   <link rel="stylesheet" href="../css/scroll.css" type="text/css" >

   <!-- syntax highlighting CSS -->
   <link rel="stylesheet" href="../css/syntax.css" type="text/css" >

   <!-- Homepage CSS -->
   <link rel="stylesheet" href="../css/screen.css" type="text/css" media="screen" >

 <!-- fontawesome CSS-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous"> 


<script defer src="https://use.fontawesome.com/releases/v5.1.0/js/v4-shims.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>

 <!-- Typekit CSS-->
<link rel="stylesheet" href="https://use.typekit.net/enh8ddt.css">
<script>
  (function(d) {
    var config = {
      kitId: 'enh8ddt',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
 <!-- Typekit -->
<!--    <script src="https://use.typekit.net/enh8ddt.js"></script>
   <script>try{Typekit.load({ async: true });}catch(e){}</script>
  <script src="https://use.typekit.com/jpd0pfm.js"></script>
   <script>try{Typekit.load();}catch(e){}</script> -->
   <!-- Paging --> 
   <script src="../scripts/paging.js"></script>
   <!-- Cookie --> 
   <script src="../scripts/cookie.js"></script>

   <!-- scrolling --> 
   <script src="../scripts/scroll.js"></script>
   <!-- timer --> 
   <script src="../scripts/timer.js"></script>
<div class="site">

  <!-- timer 
  <div class="w3-col s3">
    <a href="/"><img src="https://avatars1.githubusercontent.com/u/7329803?v=3&s=99" style="width:100%" alt=""></a>
  </div> -->
  <h1>Lisan Yordamı ile Dil Öğrenme Metodu</h1>


<span><p class="meta"><i class="fas fa-tags"></i>
  
    
    <a href="../tag/Kelime.html" style="text-decoration: none; color:#3E2723; cursor:pointer"><code class="highligher-rouge"><nobr>Kelime</nobr></code>&nbsp;</a>
  </p>
</span>









<p class="meta"><i class="far fa-clock"></i>&nbsp;11 Eylül 2018 - Türkiye</p>


<div align="right">
<a href="../index.html" title="Home"><i class="fas fa-home fa-1x " aria-hidden="true" style="color:gray;"></i></a></div>
<hr>
<br>

<style>
.alert{position:relative;padding:.75rem 1.25rem;margin-bottom:1.125rem;border:1px solid transparent;border-radius:.25rem}.alert-heading{color:inherit}.alert-link{font-weight:700}.alert-dismissible{padding-right:4.1875rem}.alert-dismissible .close{position:absolute;top:0;right:0;padding:.75rem 1.25rem;color:inherit}.alert-primary{color:#32516b;background-color:#dfebf5;border-color:#d3e3f1}.alert-primary hr{border-top-color:#c0d7eb}.alert-primary .alert-link{color:#223748}.alert-secondary{color:#7d7d7d;background-color:#fcfcfc;border-color:#fbfbfb}.alert-secondary hr{border-top-color:#eee}.alert-secondary .alert-link{color:#646464}.alert-success{color:#155724;background-color:#d4edda;border-color:#c3e6cb}.alert-success hr{border-top-color:#b1dfbb}.alert-success .alert-link{color:#0b2e13}.alert-info{color:#0c5460;background-color:#d1ecf1;border-color:#bee5eb}.alert-info hr{border-top-color:#abdde5}.alert-info .alert-link{color:#062c33}.alert-warning{color:#856404;background-color:#fff3cd;border-color:#ffeeba}.alert-warning hr{border-top-color:#ffe8a1}.alert-warning .alert-link{color:#533f03}.alert-danger{color:#850000;background-color:#fcc;border-color:#ffb8b8}.alert-danger hr{border-top-color:#ff9f9f}.alert-danger .alert-link{color:#520000}.alert-light{color:gray;background-color:#fdfdfd;border-color:#fcfcfc}.alert-light hr{border-top-color:#efefef}.alert-light .alert-link{color:#676767}.alert-dark{color:#0c1c28;background-color:#d1d7db;border-color:#bec6cd}.alert-dark hr{border-top-color:#b0b9c2}.alert-dark .alert-link{color:#000101}@-webkit-keyframes progress-bar-stripes{from{background-position:1rem 0}to{background-position:0 0}}@keyframes progress-bar-stripes{from{background-position:1rem 0}to{background-position:0 0}}

</style>

<div class="pull-right alert alert-warning" style="margin: 15px; text-align: center;">
  <img src="../images/lisan/lisan1.png" alt="lisan" class="resize" />
  <p><small>Lisan &bull; Yordam.</small></p>
</div>

<style>
img.resize {
  max-width:100%;
  max-height:100%;
}
</style>

<link href="http://fonts.googleapis.com/css?family=Life+Savers" rel="stylesheet" />

<style>
  body {
    font-family: 'Life Savers', serif;
    font-size: 1.29vw;
    } 
 .off {
    font-feature-settings: "liga" off;
    color: gray;
  }
 .on {
    font-feature-settings: "liga" on;
    color: orange;
  }
</style>

<p><span class="off">fi</span>
<span class="on">fi</span></p>

<h3 id="kelime-bilgisi-için-kaynakça">Kelime Bilgisi için Kaynakça:</h3>
<hr />

<p>Tureng Sözlük - tureng.com/tr/</p>

<p>Sesli Sözlük - seslisozluk.net</p>

<h3 id="kelimelerin-ve-cümle-içindeki-manasının-belirlenmesi">Kelimelerin ve Cümle içindeki Manasının Belirlenmesi:</h3>
<hr />

<table>
  <thead>
    <tr>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>reliable</strong> {s} güvenilir</td>
      <td>.</td>
      <td><strong>environment</strong> {i} ortam</td>
      <td>.</td>
      <td><strong>confusing</strong> {s} kafa karıştırıcı</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
    </tr>
    <tr>
      <td><strong>aspect</strong> {i} yön</td>
      <td>.</td>
      <td><strong>preserve</strong> {f} muhafaza etmek</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
    </tr>
    <tr>
      <td><strong>handling</strong> {i} kullanma</td>
      <td>.</td>
      <td><strong>concern</strong> {i} endişe</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
    </tr>
    <tr>
      <td><strong>certain</strong> {s} belirli</td>
      <td>.</td>
      <td><strong>handle</strong> {f} idare etmek</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
    </tr>
    <tr>
      <td><strong>yield</strong> {f} sağlamak</td>
      <td>.</td>
      <td><strong>conclusion</strong> {i} sonuç</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
    </tr>
    <tr>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
    </tr>
  </tbody>
</table>

<h3 id="i̇ncelenen-tümceler">İncelenen Tümceler</h3>
<hr />

<p><strong>“Datetime. In amber an insect is preserved for millions of years. A volcano erupts. A meteor hits earth. Existence in amber is unchanging.”</strong></p>

<p><strong>“In the current time (and of no concern to the insect) we use Python’s datetime module to handle dates. This module parses strings containing dates.”</strong></p>

<p><strong>“Parse. To parse we have the strptime method in datetime. The name is confusing—it comes from the C standard library. This method requires two arguments.”</strong></p>

<p><strong>“In conclusion, the Python environment has strong support for time handling. These libraries are built into the environment. They do not need to be recreated in each program.”</strong></p>

<p><strong>“This yields faster, more reliable software. Certain aspects of time handling, such as computing calendar dates by offsets, is best left to sophisticated libraries.”</strong></p>

<h3 id="tümce-çevirisi">Tümce Çevirisi</h3>
<hr />

<p>Datetime. Kehribarda bir böcek milyonlarca yıldan beri korunur. Bir volkan patlar. Bir meteor dünyaya çarpar. Amber’deki varlık değişmez.</p>

<p>Şimdiki zamanda (ve böcek için endişe duymadan) tarihleri ​​işlemek için Python’un datetime modülünü kullanıyoruz. Bu modül, tarihleri ​​idare etmek için dizeleri ayrıştırır.</p>

<p>Ayrıştırma. Ayrıştırmak için datetime’da strptime metoduna sahibiz. İsim kafa karıştırıcıdır - C standart kütüphanesinden geliyor. Bu yöntem iki argüman gerektirir.</p>

<p>Sonuç olarak, Python ortamı zaman kullanımı için güçlü bir desteğe sahiptir. Bu kütüphaneler ortam içine inşa edilmiştir. Her programda yeniden oluşturulmaları gerekmez.</p>

<p>Bu daha hızlı, daha güvenilir bir yazılım sağlar. Zamanı kullanmanın belirli yönleri, örneğin takvim tarihlerini denkleştirerek hesaplama gibi, en iyi sofistike kütüphanelere bırakılır.</p>

<h3 id="infinitive-simple-past-past-participle">Infinitive,	Simple Past,	Past Participle</h3>
<hr />

<table>
  <thead>
    <tr>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>leave</td>
      <td> </td>
      <td>left</td>
      <td> </td>
      <td>left</td>
    </tr>
    <tr>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
    </tr>
  </tbody>
</table>

<h3 id="comparison-and-superlative-adjectives">COMPARISON AND SUPERLATIVE ADJECTIVES</h3>
<hr />

<table>
  <thead>
    <tr>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
      <th> </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>good</td>
      <td> </td>
      <td>better</td>
      <td> </td>
      <td>best</td>
    </tr>
    <tr>
      <td>fast</td>
      <td> </td>
      <td>faster</td>
      <td> </td>
      <td>fastest</td>
    </tr>
    <tr>
      <td>reliable</td>
      <td> </td>
      <td>more reliable</td>
      <td> </td>
      <td>most reliable</td>
    </tr>
    <tr>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
    </tr>
  </tbody>
</table>

<div class="alert alert-warning" role="alert">
<p><strong>THE PRESENT CONTINUOUS IS USED:</strong> İngilizce'deki tüm zamanlarda olduğu gibi, konuşmacının tutumu da eylemin veya etkinliğin zamanı kadar önemlidir. Birisi present continuous kullandığında, bitmemiş ya da tamamlanmamış bir şey hakkında düşünüyor.

Şu anda devam eden bir eylemi tanımlamak için:
Bu süre boyunca devam eden bir eylemi ya da bir trendi tanımlamak için:
Önceden planlanmış veya hazırlanmış olan bir eylem veya olayı tanımlamak için:
Geçici bir olayı veya durumu açıklamak için:
Devamlı bir dizi tekrarlanan eylemi tanımlamak ve vurgulamak için 'daima, sonsuza dek, sürekli' ile:
</p>
</div>




<br>
<br>

<!---
localize date post
-->
<div align="right">







<p class="meta">Son Değişim: 11 Eylül 2018</p>

</div>
<hr>
<br>
<br>
<!---
latest post
-->







<h3>En Yeni İçerikler</h3>
<ul class="posts">
    
    
      <li><span>29 May 2021</span> &raquo; <a href="../web/2021/05/29/Pre-Browsing.html" style="text-decoration: none; color:#3E2723; cursor:pointer">Yerelleştirme : Prefetching, Preloading, Prerendering</a></li>
    
      <li><span>27 May 2021</span> &raquo; <a href="../kitap/2021/05/27/Atom-Mucizesi.html" style="text-decoration: none; color:#3E2723; cursor:pointer">Atom Mucizesi</a></li>
    
</ul>


<!---
relatedPosts
-->  
<div class="relatedPosts">






  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  
    
    

    

    
  

  
  





  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
    
    

    

  <ul class="posts">
    

  
  </ul>
</div>

<!---
page.comments
--> 


<div class="teaser clearfix"></div>
  <div class="footer">
<br/> 
    <div class="contact">

      
      <a href="https://github.com/vdemir" title="Github" target="_blank"><i class="fab fa-github fa-4x " aria-hidden="true"></i></a>

      <a href="https://www.youtube.com/channel/UCghWuRqdO-HXsxRxBn4dHHQ/videos" title="YouTube" target="_blank"><i class="fab fa-youtube fa-4x " aria-hidden="true"></i></a>

      <a href="../feed.xml" title="RSS" target="_blank"><i class="fas fa-rss fa-4x " aria-hidden="true"></i></a>

      <a href="https://jigsaw.w3.org/css-validator/validator?uri=vdemir.github.io" title="Valid CSS!" target="_blank"><i class="fab fa-css3-alt fa-4x " aria-hidden="true"></i></a>

      <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.tr" title="Creative Commons License" target="_blank"><i class="fas fa-closed-captioning fa-4x " aria-hidden="true"></i></a>
 
    </div>
   
<a href="../index.html" title="Home"><i class="fas fa-home fa-3x " aria-hidden="true" style="color:gray;"></i></a>


    </div>
</div>

<noscript>
    <img src="https://counter2000.herokuapp.com/ingress/2de72512-47ff-4d6f-ab2b-99c87373394b/pixel.gif" alt="counter" title="counter">
</noscript>
<script defer src="https://counter2000.herokuapp.com/ingress/2de72512-47ff-4d6f-ab2b-99c87373394b/script.js"></script>

</body>

<!-- Mirrored from vdemir.github.io/Lisan/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Jun 2021 13:12:59 GMT -->
</html>
